#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cmath>
using namespace std;

const int SIZE = 10;
void printboarder();
void dispintmessage();
int createarray(int **arr, int *numants);
void printarray(int **arr, int curcolony);
bool checkstatus(int row, int colony, int *numants, int **arr, int *visitedrow);
enum Colony{A=0, B=1, C=2, D=3, E=4, F=5, G=6, H=7, I=8, J=9};

int main()
{
        printboarder();
        dispintmessage();
	cout << endl;
        int numants;
        int **arr;
        arr = new int*[SIZE];
        arr = (int**)malloc(10*sizeof(int*));
		int curcolony = B;
		for (int i=0; i<SIZE; i++)
        {
                arr[i]=new int[SIZE];
                arr[i]=(int*)malloc(10*sizeof(int));
        }
        int userrow;
        int i=0;
        int visitedcolony[10];
        bool status;
        numants=createarray(arr, &numants);
        printarray(arr, curcolony);
        cout << "Time to begin. Anthony has " << numants << " ants in his army." << endl;
	while((curcolony<=J)&(numants>0))
        {
	  	cout << "Enter the row in column " << curcolony << " to attack: " ;
                cin >> userrow;
                if((userrow>=0)&(userrow<=9))
                {
                        status=checkstatus(userrow, curcolony, &numants, arr, visitedcolony);
                        if(status == true)
                        {
                                int colonyants = 0;
                        	for(int i=0; i<SIZE; i++)
                        	{
                       	    		if((arr[i][curcolony]!=0)&(arr[i][curcolony]!='X'))
                        		{
                              	 		colonyants = colonyants+arr[i][curcolony];
                          		}
                        	}
                        	numants+=colonyants;

                        	curcolony = curcolony + 1;
				visitedcolony[i]=userrow;
				i++;
                        }
			 printarray(arr, curcolony);


                }
                else
                {
                        cout << "Invalid selection. Please enter a row number between 0 and 9 inclusive." << endl;
                }
        }
	if(curcolony>J)
	{
		cout << "Congratulations! You made it to the end of the game." << endl;
	}
	if(numants<0)
	{
		cout << "Unfortunately, Anthony's army has been defeated. There are zero ants left." << endl;
	}
        delete[]arr[i];
        delete[] arr;
        return 0;
}
void printboarder()
{
        string border( 52, '-');
        string spaces( 52, ' ' );
        string spa (10, ' ');
        string spac (11, ' ');
        string space (7, ' ');
        string sp(5, ' ');

        cout << " " << endl;cout << '+' << border << "+\n";
        cout << '|' << spa << "Computer Science and Engineering" << spa << "|\n";
        cout << '|' << spac << "CSCE 1030 - Computer Science I" << spac << "|\n";
        cout << '|' << space << "Aima Ovai  ao0323  aimaovai@my.unt.edu" << space << "|\n";
        cout << '|' << spac << "Date: 12/2/2019   Section:003" << spac << "|\n";
        cout << '+' << border << "+\n";
        cout << " " << endl;
}
void dispintmessage()
{
	cout << "                      Welcome to Anthony's battle with the ants!                         " << endl;
	cout << "---------------------------------------------------------------------------------------" << endl;
	cout << "Anthony the ant has decided that instead of following the pheromone levels as he did in" << endl;
	cout << "Project 3, he wants to take over all of the adjacent ant colonies with his army of ants" << endl;
	cout << "in Colony ‘A’. Therefore, in this C++ program, you will create a matrix (two-dimensional" << endl;
	cout << "array) as you did in Project 3, except that it will be done dynamically. One difference," << endl;
	cout << "however, is that the columns will represent each ant colony neighborhood (i.e., ‘A’" << endl;
	cout << "through ‘J’) consisting of one ant colony and nine ant patrols (i.e., 1 to 10 ants," << endl;
	cout << "inclusively, deployed nearby to protect the ant colony). Then, you will have Anthony’s" << endl;
	cout << "army attempt to take over the adjacent ant colonies, one column at a time, capturing" << endl;
	cout << "ants (and adding to Anthony’s army) along the way. If Anthony and his army can get to" << endl;
	cout << "the ant colony for column ‘J’ with at least 1 ant remaining, then Anthony and his army" << endl;
	cout << "will have successfully taken over the adjacent ant colonies." << endl;
	cout << "-----------------------------------------------------------------------------------------" << endl;

	cout << "Getting system ready..." << endl;
}
int createarray(int **arr, int *numants)
{
        srand(time(NULL));//seeding the random numbers
        *numants=0;//initialize the number of ants to zero
        int col[10];
        int j;
        int i;
        int b[10];

         for(int i=0; i<SIZE; i++)
        {
                for(j=0; j<SIZE; j++)
                {
                        arr[i][j]=rand()%10 + 1;
                }
        *numants += *arr[i];//gets sum of all ants in column one as starting number of ants in the army
        }
        for(int j=0; j<SIZE; j++)
        {
        	i = rand()%10;
			if(i!=b[j])
			{
				b[j] = i;
				arr[i][j]=0;
			}
	}
	
        
         return *numants;
}

void printarray(int **arr, int curcolony)
{
        int matrow=0;
cout << "   A  B  C  D  E  F  G  H  I  J" << endl;
cout << " +-------------------------------+" << endl;
int j=0;
for(int i=0; i<SIZE; i++)
{
        int A=0;
         cout << matrow << "| ";

        if((log10(arr[i][j])+1)<2)//checks for length of integer
        {
                cout  << A << arr[i][j];
        }
        else
        {
                cout << arr[i][j];
        }
	for(j=1; j<curcolony; j++)
	{
        if(arr[i][curcolony]==-1)//checks if colony has been visited to print out "XX"
	{
   		cout << " XX" ;
	}
	else if((arr[i][curcolony]!=-1)&(arr[i][curcolony]==arr[i][j]))//checks if colony value is still the same as the initial value after the colonyhas been updated, and prits out the actual value
	{
		cout << arr[i][j];
	}
        else
        {
            	cout << " **";
	}
	}
                    
                for(int j=1; j<SIZE; j++)//print initial colony matrix
                {
                    	cout << " **";
                }
        cout << " |" << endl;
        matrow++;
}
                cout << " +-------------------------------+" << endl;
}
bool checkstatus(int row, int colony, int *numants, int **arr, int *visitedrow)
{
        
        if(arr[colony][row]==0)//checks if the value at the index is 0
        {
                return true;
        }
        
        for(int i=0; i<10; i++)//iterates through each value storred in the visited row array
        {
                if(visitedrow[i]==row)//checks if the vlaue entered is equal to the value stored
                {
                        cout << "This patrol has already been visited." << endl;//display for if a value has been entered already
                        return false;
                }
               
        }
        for(int i=0; i<SIZE; i++)
        {
       		 if((arr[row][colony]!=0)&(row!=visitedrow[i]))//checks if the value of 2d matrix at index is not zero and not equal to any value already entered
        	{
    	            *numants = *numants - arr[i][colony];//subtracts the number of ants in that row from total number of ants in Anthony's army and reassigns numants with the value
        	        arr[i][colony] = -1;//assigns each visited index value with value of -1
        	        cout << "Anthony's army now has " << *numants << " ants left." << endl;//display how many ants are left in Anthony's army
                	return false;
            }
        }
        
    
}
